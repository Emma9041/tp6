from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
from client.main import getMots

class Mot(BaseModel):
    id:int
    caracteres:str
    def __init__(self,id,caracteres):
        self.id=id
        self.caracteres=caracteres


app = FastAPI()


@app.get("/")
def read_root():
    return "Mots"

@app.post("/mot/")
async def create_mot(mot: Mot):
    return mot


@app.get("/mots")
def read_mots():
    return {"mots": getMots()}

